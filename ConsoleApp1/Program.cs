﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Writing;

namespace ConsoleApp1
{
    internal class Program
    {
        private const string QuerySelect = "SELECT DISTINCT ?s ?p ?o { ?s ?p ?o }";
        private const string QueryInsert = "PREFIX oa: <http://www.w3.org/ns/oa#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> INSERT DATA{ [ a oa:Annotation ; rdfs:label \"Title\" ; ] .}";

        private static void Main(string[] args)
        {
            try
            {
                FusekiConnector conn = new FusekiConnector(new Uri("http://145.239.82.37:3030/testDB/data"));

                PerformInsertQuery(conn, QueryInsert);
                PerformSelectQuery(conn, QuerySelect);
            }
            catch (Exception ex)
            {
                PrintExceptionMessage(ex);
            }

            Console.ReadKey();
        }

        private static void PrintExceptionMessage(Exception ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.InnerException != null)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        private static void PerformSelectQuery(FusekiConnector conn, string query)
        {
            SparqlResultSet results = (SparqlResultSet)conn.Query(query);

            foreach (SparqlResult r in results)
            {
                Console.WriteLine(r["s"] + " " + r["p"] + " " + r["o"]);
            }
        }

        private static void PerformInsertQuery(FusekiConnector conn, string query)
        {
            if (!conn.UpdateSupported)
            {
                return;
            }

            conn.Update(query);

            //Another way to insert data
            //Graph g = new Graph();
            //INode s = g.CreateBlankNode("a");
            //INode p = g.CreateUriNode(new Uri("http://www.w3.org/ns/oa#Annotation"));
            //INode o = g.CreateLiteralNode("Title2", new Uri("http://www.w3.org/2000/01/rdf-schema#label"));
            //Triple t = new Triple(s, p, o);

            //conn.UpdateGraph(string.Empty, new List<Triple> { t }, null);
        }
    }
}